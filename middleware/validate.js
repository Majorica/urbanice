const { param, body, validationResult } = require('express-validator');
const response = require('./../config/response')


function handleError (req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(200).json( response.validateError(errors.errors) );
  }

  next();
}

exports.sentMail = [

  body('from').isEmail(),
  body('to').isEmail(),
  body('message').notEmpty().isString(),
  handleError

]

exports.contactGroup = [
  body('name').notEmpty().isString(),
  handleError
]

exports.contactGroupUpdate = [
  param('id').notEmpty(),
  body('name').notEmpty().isString(),
  handleError
]

exports.contactGroupDelete = [
  param('id').notEmpty(),
  handleError
]

exports.contact = [

  body('first_name').notEmpty().isString(),
  // body('last_name').isString(),
  // body('birth_date').isString(),
  // body('phone.*').isString().optional({ nullable: true }),
  // body('email.*').isEmail().optional({ nullable: true }),
  // body('url').isURL().optional({ nullable: true }),
  // handleError

]

exports.contactUpdate = [
  param('id').notEmpty(),
  body('first_name').notEmpty().isString(),
  body('last_name').isString(),
  body('birth_date').isString(),
  body('phone.*').isString().optional({ nullable: true }),
  body('email.*').isEmail().optional({ nullable: true }),
  body('url').isURL().optional({ nullable: true }),
  handleError
]

exports.contactDelete = [
  param('id').notEmpty(),
  handleError
]

exports.taxCalculate = [
  body('net_income').isNumeric(),
  handleError
]