const sgMail = require('@sendgrid/mail')
const config = require('./../config/config').getConfig()

sgMail.setApiKey( config.SENDGRID_API_KEY )

/**
 * @param payload.to : String, your recipient
 * @param payload.from : String, your verified sender
 * @param payload.subject : String
 * @param payload.text : String
 * @param payload.html : String, <p>Can use html element</p>
 */
module.exports.send = ( payload ) => {

  return new Promise( (resolve, reject) => {
    sgMail
    .send( payload )
    .then((data) => {
      resolve(data)
    })
    .catch((error) => {
      console.log(error);
      reject(error)
    })
  })
  
}


