const express = require('express')
const router = express.Router()
const validate = require('./../middleware/validate');

const as1 = require('./../controller/assignment1');
const as2 = require('./../controller/assignment2');
const as3 = require('./../controller/assignment3');

router.post('/sent-mail', validate.sentMail, as1.sent_mail )

router.get('/contact-group', as2.getContactGroup )
router.post('/contact-group', validate.contactGroup, as2.createContractGroup )
router.put('/contact-group/:id', validate.contactGroupUpdate, as2.updateContractGroup )
router.delete('/contact-group/:id', validate.contactGroupDelete, as2.deleteContractGroup )

router.post('/contacts', validate.contact, as2.createContact  )
router.put('/contacts/:id', validate.contactUpdate, as2.updateContact  )
router.put('/contacts/:id/profile', as2.updateProfile  )
router.delete('/contacts/:id', validate.contactDelete, as2.deleteContact  )

router.post('/tax-calculator', validate.taxCalculate, as3.taxCalculate )

module.exports = router
