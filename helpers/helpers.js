const fs = require("fs");
const gm = require("gm").subClass({ imageMagick: true });
const path = require("path");
const uuidv4 = require("uuid");

module.exports = {
    //* อัพโหลดไฟล์
  async uploadFile(
    pathToFile,
    file,
    resize = false,
    width = null,
    height = null
  ) {
    //หา path จริงของโปรเจค
    const projectPath = path.resolve("./");
    //โฟลเดอร์และ path ของการอัปโหลด
    const uploadPath = `${projectPath}/uploads/${pathToFile}`;
    if (!fs.existsSync(uploadPath)) fs.mkdirSync(uploadPath);

    let resizePath;

    if (resize === true) {
      //โฟลเดอร์และ path ของการอัปโหลดรูปที่ resize
      resizePath = `${projectPath}/uploads/${pathToFile}/thumbnail`;
      if (!fs.existsSync(resizePath)) fs.mkdirSync(resizePath);
    }

    const ext = file.name.split(".").pop(); //หานามสกุลไฟล์
    const newFileName = uuidv4.v4() + "." + ext;
    await file.mv(`${projectPath}/uploads/${pathToFile}/${newFileName}`);

    if (resize === true) {
      //แปลงให้เป็น promise
      const resize = function (
        uploadPath,
        uploadResizePath,
        fileName,
        width,
        height
      ) {
        return new Promise(function (resolve, reject) {
          //options https://aheckmann.github.io/gm/docs.html
          //%, @, !
          gm(`${uploadPath}/${fileName}`)
            .resize(width, height)
            .write(`${uploadResizePath}/${fileName}`, (err) => {
              if (err) reject(err);
              else resolve();
            });
        });
      };

      //resize รูป
      await resize(uploadPath, resizePath, newFileName, width, height);
    }
    //return ชื่อไฟล์ใหม่ออกไป
    return newFileName;
  },
};
