require('dotenv').config()
const fileUpload = require("express-fileupload");
const express = require('express')
const app = express()
const bodyParser = require('body-parser')

const config = require('./config/config').getConfig()

app.use(express.static("uploads"));
app.use(bodyParser.json());

app.use(
  fileUpload({
    limits: { fileSize: 5 * 1024 * 1024 }, //limit size 5 MB
  })
);



const port = config.PORT;

app.use('/', require('./routes/index'))

app.listen(port, () => {
  console.log(`App listening on port ${port}`)
})
