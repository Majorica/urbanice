const fs = require('fs');
const path = require('path');

const response = require('./../config/response');
const $helpers = require('./../helpers/helpers');

const filelPath = './db.json';
const file = require(filelPath);

Date.prototype.isValid = function () {
  return this.getTime() === this.getTime();
};  

function getJsonDB() {
  return JSON.parse(fs.readFileSync( path.join(__dirname, filelPath), 'utf8'))
}

class Assignment2 {

  getContactGroup (req, res) {
    let contractGroup = getJsonDB().contact_group
    return res.status(200).json( response.successData( contractGroup, 'Get contacts group success' ) )
  }

  createContractGroup (req, res) {

    let tb_name = "contact_group"
    let contractGroup = getJsonDB()
    let last_index = contractGroup[tb_name].length;

    let payload = {
      id: last_index == 0 ? 1: contractGroup[tb_name][ last_index-1].id+1,
      name: req.body.name,
      contacts: []
    }

    contractGroup[tb_name].push(payload)

    fs.writeFileSync(path.join(__dirname, filelPath), JSON.stringify(contractGroup));

    return res.status(200).json( response.successData(payload, 'Create contact group success') )
    
  }

  updateContractGroup (req, res) {

    let tb_name = "contact_group"
    let contractGroup = getJsonDB()
    let update_index = null;

    for (let i=0;i< contractGroup[tb_name].length; i++) {
      if ( contractGroup[tb_name][i].id == req.params.id ) {
        update_index = i;
        break;
      }
    }

    if ( update_index == null ) {
      return res.status(200).json( response.errorPayload('Invalid group id') )
    }

    contractGroup[tb_name][update_index].name = req.body.name;
    fs.writeFileSync(path.join(__dirname, filelPath), JSON.stringify(contractGroup));
    return res.status(200).json( response.successData( contractGroup[tb_name][update_index], 'Update successfully.') )
    
  }

  deleteContractGroup (req, res) {
    
    let tb_name = "contact_group"
    let contractGroup = getJsonDB()
    let delete_index = null;

    for (let i=0;i< contractGroup[tb_name].length; i++) {
      if ( contractGroup[tb_name][i].id == req.params.id ) {

        for (let j=0; j< contractGroup[tb_name][i].contacts.length; j++ ) {
          fs.unlinkSync(path.join(__dirname, './../uploads/'+ contractGroup[tb_name][i].contacts[j].photo)  )
        }
        delete_index = i;
        break;
      }
    }

    if ( delete_index == null ) {
      return res.status(200).json( response.errorPayload('Invalid group id') )
    }

    contractGroup[tb_name].splice(delete_index, 1)

    fs.writeFileSync(path.join(__dirname, filelPath), JSON.stringify(contractGroup));
    return res.status(200).json( response.success( 'Delete successfully.') )
    
  }

  async createContact (req, res) {


    let birth_date = new Date(req.body.birth_date)

    if ( !birth_date.isValid() ) {
      return res.status(200).json( response.errorPayload(`birth_date is not a Date format. (Example: '2022/1/15 13:40:12 GMT+0700')`) )
    }


    let tb_name = "contact_group"
    let contractGroup = getJsonDB()

    var fileName;
    if ( req.files ) {
      fileName = await $helpers.uploadFile(
        `/profile`,
        req.files.image,
        false
      );
    }
    
    let payload = {
      id: ++contractGroup.lastContactID,
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      birth_date: new Date(req.body.birth_date),
      phone: req.body.phone,
      email: req.body.email,
      url: req.body.url,
      photo: '/profile/' +fileName
    }

    let update_index = null;

    for (let i=0;i< contractGroup[tb_name].length; i++) {
      if ( contractGroup[tb_name][i].id == req.body.group_id ) {
        update_index = i;
        break;
      }
    }

    if ( update_index == null ) {
      return res.status(200).json( response.errorPayload('Invalid group id') )
    }

    contractGroup[tb_name][update_index].contacts.push( payload )

    fs.writeFileSync(path.join(__dirname, filelPath), JSON.stringify(contractGroup));

    return res.status(200).json( response.successData( payload, 'Create success fully.') )
    

  }

  updateContact (req, res) {

    let birth_date = new Date(req.body.birth_date)

    if ( !birth_date.isValid() ) {
      return res.status(200).json( response.errorPayload(`birth_date is not a Date format. (Example: '2022/1/15 13:40:12 GMT+0700')`) )
    }

    let tb_name = "contact_group"
    let contractGroup = getJsonDB()
    

    let update_index = null;
    let contact_index = null;

    for (let i=0;i< contractGroup[tb_name].length; i++) {
      if ( contractGroup[tb_name][i].id == req.body.group_id ) {
        update_index = i;

        let ct = contractGroup[tb_name][i].contacts;
        
        for (let j=0; j< ct.length; j++) {
          if ( ct[j].id == req.params.id ) {
            contact_index = j;
            break;
          }
        }

        break;
      }
    }

    if ( update_index == null ) {
      return res.status(200).json( response.errorPayload('Invalid group_id') )
    }

    if ( contact_index == null ) {
      return res.status(200).json( response.errorPayload('Invalid id') )
    }

    let payload = {
      id: contractGroup[tb_name][update_index].contacts[ contact_index ].id,
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      birth_date: new Date(req.body.birth_date),
      phone: req.body.phone,
      email: req.body.email,
      url: req.body.url
    }

    contractGroup[tb_name][update_index].contacts[ contact_index ] = payload
    
    fs.writeFileSync(path.join(__dirname, filelPath), JSON.stringify(contractGroup));
    return res.status(200).json( response.successData( contractGroup[tb_name][update_index].contacts[contact_index], 'Update successfully.') )
    
  }

  async updateProfile (req, res) {

    if ( !req.files ) {
      return res.status(200).json( response.errorPayload(`Please upload profile image at key 'image'`) )
    } 

    let fileName = await $helpers.uploadFile(
      `/profile`,
      req.files.image,
      false
    );

    let tb_name = "contact_group"
    let contractGroup = getJsonDB()
    
    let update_index = null;
    let contact_index = null;

    for (let i=0;i< contractGroup[tb_name].length; i++) {
        
      let ct = contractGroup[tb_name][i].contacts;
      
      for (let j=0; j< ct.length; j++) {
        update_index = i;
        if ( ct[j].id == req.params.id ) {
          contact_index = j;
          break;
        }
      }

     
    }

    if ( update_index == null ) {
      return res.status(200).json( response.errorPayload('Invalid group_id') )
    }

    if ( contact_index == null ) {
      return res.status(200).json( response.errorPayload('Invalid id') )
    }

    fs.unlinkSync(path.join(__dirname, './../uploads/'+ contractGroup[tb_name][update_index].contacts[ contact_index ].photo )  )

    contractGroup[tb_name][update_index].contacts[ contact_index ].photo = '/profile/' +fileName
    
    fs.writeFileSync(path.join(__dirname, filelPath), JSON.stringify(contractGroup));
    return res.status(200).json( response.successData( contractGroup[tb_name][update_index].contacts[contact_index], 'Update successfully.') )
    
  }

  deleteContact (req, res) {

   
    let tb_name = "contact_group"
    let contractGroup = getJsonDB()

    let update_index = null;
    let contact_index = null;

    for (let i=0;i< contractGroup[tb_name].length; i++) {

        let ct = contractGroup[tb_name][i].contacts;
        
        for (let j=0; j< ct.length; j++) {
          if ( ct[j].id == req.params.id ) {
            update_index = i;
            contact_index = j;
            break;
          }
        }

      
    }

    if ( contact_index == null ) {
      return res.status(200).json( response.errorPayload('Invalid id') )
    }


    fs.unlinkSync(path.join(__dirname, './../uploads/'+ contractGroup[tb_name][update_index].contacts[contact_index].photo)  )

    contractGroup[tb_name][update_index].contacts.splice(contact_index, 1)
    
    fs.writeFileSync(path.join(__dirname, filelPath), JSON.stringify(contractGroup));
    return res.status(200).json( response.success( 'Delete successfully.') )
    
  }

}

module.exports = new Assignment2()