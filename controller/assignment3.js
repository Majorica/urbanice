const response = require('./../config/response')

class Assignment3 {

  async taxCalculate (req, res) {

    let net_income = req.body.net_income;
    let percent = 0;

    if ( net_income <= 150000 ) {
      percent = 0;
    }  else if ( net_income <= 300000 ) {
      percent = 5
    }  else if ( net_income <= 500000 ) {
      percent = 10
    }  else if ( net_income <= 750000 ) {
      percent = 15
    }  else if ( net_income <= 1000000 ) {
      percent = 15
    }  else if ( net_income <= 2000000 ) {
      percent = 25
    }  else if ( net_income <= 5000000 ) {
      percent = 30
    }  else {
      percent = 35
    } 

    const options_number = {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    };

    let msg = "";
    let tax = net_income/100*percent

    if ( percent == 0 ) {
      msg = "ได้รับการยกเว้นภาษี"
    } else {
      msg = `ภาษีที่ต้องชำระ ${Number(tax).toLocaleString('en', options_number)}`
    }

    return res.status(200).json( response.success( msg ) )
    
  }

}

module.exports = new Assignment3()