const sendgrid = require('./../service/sendgrid')
const response = require('./../config/response')

class Assignment1 {

  async sent_mail (req, res) {
    try {

      await sendgrid.send({
        to: req.body.to,
        from: req.body.from,
        subject: 'Hi Urbanice',
        text: req.body.message,
        html: 'Hello world !',
      });
    
      return res.status(200).json( response.success('Sent mail success.') )
      
    } catch (error) {
      console.log(error)
      return res.status(200).json( response.unknowError() )
    }
  }

}

module.exports = new Assignment1()