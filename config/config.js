module.exports.getConfig = () => {
  const config = {
    'PORT': process.env.PORT || 3000,
    'SENDGRID_API_KEY': process.env.SENDGRID_API_KEY || null,
  };
  
  return config;
};
