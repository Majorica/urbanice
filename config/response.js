/**
 * @param msg : String
 */
module.exports.success = ( msg = null ) => {
  return {
    success: true,
    message: !msg ? 'Success.' : msg
  }
}

/**
 * @param data : Array
 * @param msg : String
 */
module.exports.successData = ( data = [], msg = null ) => {
  return {
    success: true,
    message: !msg ? 'Success.' : msg,
    data
  }
}

module.exports.unknowError = () => {
  return {
    success: false,
    errorCode: "0",
    message: "Something went wrong, Please try again next time."
  }
}

/**
 * @param errors : Array
 */
module.exports.validateError = (errors) => {
  return {
    success: false,
    errorCode: "1",
    message: "Validate error !",
    errors: errors
  }
}

/**
 * @param msg : String
 */
module.exports.errorPayload = (msg = null) => {
  return {
    success: false,
    errorCode: "2",
    message: !msg ? 'Wrong payload.' : msg
  }
}
